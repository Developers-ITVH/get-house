package com.mx.bringsolutions.gethome.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mx.bringsolutions.gethome.MasInformacion;
import com.mx.bringsolutions.gethome.Objetos.Hogar;
import com.mx.bringsolutions.gethome.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterHogar extends RecyclerView.Adapter<AdapterHogar.ViewHolder>
{
    public List<Hogar> hogarListr;
    public Context context;



    public AdapterHogar(List<Hogar> ventaList, Context context)
    {
        this.hogarListr = ventaList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjetahogar,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i)
    {
        viewHolder.titulo.setText(hogarListr.get(i).getTitulo());
        viewHolder.lugar.setText(hogarListr.get(i).getLugar());
        viewHolder.descripcion.setText(hogarListr.get(i).getDescripcion());
        viewHolder.precio.setText("$ " + " " +hogarListr.get(i).getPrecio());
        Picasso.with(context).load(hogarListr.get(i).getFoto()).resize(500,500).error(R.drawable.logo).centerInside().into(viewHolder.foto);
        viewHolder.masinformacion.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Bundle bundle = new Bundle();

                Intent intent = new Intent(context, MasInformacion.class);
                intent.putExtra("titulo",hogarListr.get(i).getTitulo());
                intent.putExtra("lugar",hogarListr.get(i).getLugar());
                intent.putExtra("descripcion",hogarListr.get(i).getDescripcion());
                intent.putExtra("precio",String.valueOf(hogarListr.get(i).getPrecio()));
                intent.putExtra("foto",hogarListr.get(i).getFoto());

                context.startActivity(intent);

            }
        });

        viewHolder.contactar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String dial = "tel:" + "9933951421";
                context.startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(dial)));
            }
        });

    }

    @Override
    public int getItemCount()
    {
        return hogarListr.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        private TextView titulo,lugar,descripcion,precio,masinformacion;
        private Button contactar;
        private CardView ventas;
        private ImageView foto;



        public ViewHolder(View itemView)
        {
            super(itemView);
            //enalanzando elementos

            masinformacion = itemView.findViewById(R.id.vermas);
            titulo      = itemView.findViewById(R.id.titulo);
            lugar       = itemView.findViewById(R.id.lugar);
            descripcion = itemView.findViewById(R.id.descripcion);
            precio      = itemView.findViewById(R.id.precio);
            foto        = itemView.findViewById(R.id.imgfoto);
            contactar   = itemView.findViewById(R.id.contactar);



        }
    }
}
