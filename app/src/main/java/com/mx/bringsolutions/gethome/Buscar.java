package com.mx.bringsolutions.gethome;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mx.bringsolutions.gethome.Adapters.AdapterHogar;
import com.mx.bringsolutions.gethome.Objetos.Hogar;

import java.time.Year;
import java.util.ArrayList;
import java.util.List;

import static java.security.AccessController.getContext;

public class Buscar extends AppCompatActivity
{



    RecyclerView recyclerView;
    private AdapterHogar adapter;
    List<Hogar> HOGAR = new ArrayList<>() ;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar);
        getSupportActionBar().hide();

        recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
       // Hogar.add(Constantes.YEAR);

        String imagen = "https://www.lamudi.com.mx/static/media/bm9uZS9ub25l/2x2x2x380x244/4da9bfe7d17111.jpg";
        HOGAR.add(new Hogar("Departamento en Renta","Bonito departamento en renta en gaviotas norte",1,imagen,1.500,2,"Gaviotas Sur"));
        HOGAR.add(new Hogar("Departamento en Renta","Bonito departamento en renta en gaviotas norte",1,imagen,1.500,2,"Gaviotas Sur"));
        HOGAR.add(new Hogar("Departamento en Renta","Bonito departamento en renta en gaviotas norte",1,imagen,1.500,2,"Gaviotas Sur"));
        HOGAR.add(new Hogar("Departamento en Renta","Bonito departamento en renta en gaviotas norte",1,imagen,1.500,2,"Gaviotas Sur"));
        HOGAR.add(new Hogar("Departamento en Renta","Bonito departamento en renta en gaviotas norte",1,imagen,1.500,2,"Gaviotas Sur"));
        HOGAR.add(new Hogar("Departamento en Renta","Bonito departamento en renta en gaviotas norte",1,imagen,1.500,2,"Gaviotas Sur"));
        HOGAR.add(new Hogar("Departamento en Renta","Bonito departamento en renta en gaviotas norte",1,imagen,1.500,2,"Gaviotas Sur"));


        adapter = new AdapterHogar(HOGAR,getApplicationContext());
        recyclerView.setAdapter(adapter);
    }
}
