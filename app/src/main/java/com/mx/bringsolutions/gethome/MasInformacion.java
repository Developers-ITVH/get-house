package com.mx.bringsolutions.gethome;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.nio.file.Files;

public class MasInformacion extends AppCompatActivity
{

    WebView webView;
    TextView tvtitulo,tvlugar,tvdescripcion,tvprecio;
    ImageView img1,img2,img3;
    Button contactar;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mas_informacion);
        getSupportActionBar().hide();

        webView   = (WebView) findViewById(R.id.webview);
        tvtitulo  = findViewById(R.id.titulo);
        tvlugar   = findViewById(R.id.lugar);
        tvprecio  = findViewById(R.id.precio);
        img1    = findViewById(R.id.button2);
        img2    = findViewById(R.id.button3);
        img3    = findViewById(R.id.button4);
        tvdescripcion = findViewById(R.id.descripcion);
        contactar   = findViewById(R.id.contactar);


        Bundle parametros = this.getIntent().getExtras();
        String titulo = parametros.getString("titulo");
        String lugar = parametros.getString("lugar");
        String descripcion = parametros.getString("descripcion");
        String precio = parametros.getString("precio");
        String foto = parametros.getString("foto");



        tvtitulo.setText(titulo);
        tvdescripcion.setText(descripcion);
        tvlugar.setText(lugar);
        tvprecio.setText("$ " +precio);
        Picasso.with(getApplicationContext()).load(foto).resize(500,500).error(R.drawable.logo).centerInside().into(img1);
        Picasso.with(getApplicationContext()).load(foto).resize(500,500).error(R.drawable.logo).centerInside().into(img2);
        Picasso.with(getApplicationContext()).load(foto).resize(500,500).error(R.drawable.logo).centerInside().into(img3);




        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);


        String url = "https://www.google.com.mx/maps/place/" + lugar;

        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient());
        contactar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String dial = "tel:" + "9933951421";
                startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(dial)));
            }
        });
    }
}
