package com.mx.bringsolutions.gethome.Objetos;

public class Hogar

{
    String titulo;
    String descripcion;
    int tipo ;
    String foto;
    double precio;
    int tipo_trato;
    String lugar;


    public Hogar()
    {
    }

    public Hogar(String titulo, String descripcion, int tipo, String foto, double precio, int tipo_trato,String lugar)
    {
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.tipo = tipo;
        this.foto = foto;
        this.precio = precio;
        this.tipo_trato = tipo_trato;
        this.lugar=lugar;
    }

    public String getLugar()
    {
        return lugar;
    }

    public void setLugar(String lugar)
    {
        this.lugar = lugar;
    }

    public double getPrecio()
    {
        return precio;
    }

    public void setPrecio(double precio)
    {
        this.precio = precio;
    }

    public int getTipo_trato()
    {
        return tipo_trato;
    }

    public void setTipo_trato(int tipo_trato)
    {
        this.tipo_trato = tipo_trato;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public int getTipo()
    {
        return tipo;
    }

    public void setTipo(int tipo)
    {
        this.tipo = tipo;
    }

    public String getFoto()
    {
        return foto;
    }

    public void setFoto(String foto)
    {
        this.foto = foto;
    }
}
