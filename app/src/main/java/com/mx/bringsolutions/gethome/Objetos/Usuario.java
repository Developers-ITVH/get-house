package com.mx.bringsolutions.gethome.Objetos;

public class Usuario
{
    String nick;
    String password;
    int tipo ;

    public Usuario()
    {
    }

    public Usuario(String nick, String password, int tipo)
    {
        this.nick = nick;
        this.password = password;
        this.tipo = tipo;
    }

    public String getNick()
    {
        return nick;
    }

    public void setNick(String nick)
    {
        this.nick = nick;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public int getTipo()
    {
        return tipo;
    }

    public void setTipo(int tipo)
    {
        this.tipo = tipo;
    }
}
